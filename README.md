# Ransack Query Builder 

[Ransack](https://github.com/activerecord-hackery/ransack)

# How to use?

see [example](https://gitlab.com/macwac/ransack-query-builder/blob/master/examples/example.ts)

# Matchers
| Filter | Match to | Description | Notes |
| --- | --- | --- | --- |
| eq | `*_eq`  | equal  | |
| notEq | `*_not_eq` | not equal | |
| matches | `*_matches` | matches with `LIKE` | e.g. `q[email_matches]=%@gmail.com`|
| doesNotMatch | `*_does_not_match` | does not match with `LIKE` | |
| matchesAny | `*_matches_any` | Matches any | |
| matchesAll | `*_matches_all` | Matches all  | |
| doesNotMatchAny | `*_does_not_match_any` | Does not match any | |
| doesNotMatchAll | `*_does_not_match_all` | Does not match all | |
| lt | `*_lt` | less than | |
| ltEq | `*_lteq` | less than or equal | |
| gt | `*_gt` | greater than | |
| gtEq | `*_gteq` | greater than or equal | |
| present | `*_present` | not null and not empty | Only compatible with string columns. Example: `q[name_present]=1` (SQL: `col is not null AND col != ''`) |
| blank | `*_blank` | is null or empty. | (SQL: `col is null OR col = ''`) |
| null | `*_null` | is null | |
| notNull | `*_not_null` | is not null | |
| in | `*_in` | match any values in array | e.g. `q[name_in][]=Alice&q[name_in][]=Bob` |
| notIn | `*_not_in` | match none of values in array | |
| ltAny | `*_lt_any` | Less than any |  SQL: `col < value1 OR col < value2` |
| ltEqAny | `*_lteq_any` | Less than or equal to any | |
| gtAny | `*_gt_any` | Greater than any | |
| gtEqAny | `*_gteq_any` | Greater than or equal to any | |
| ltAll | `*_lt_all` | Less than all | SQL: `col < value1 AND col < value2` |
| ltEqAll | `*_lteq_all` | Less than or equal to all | |
| gtAll | `*_gt_all` | Greater than all | |
| gtEqAll | `*_gteq_all` | Greater than or equal to all | |
| notEqAll | `*_not_eq_all` | none of values in a set | |
| start | `*_start` | Starts with | SQL: `col LIKE 'value%'` |
| notStart | `*_not_start` | Does not start with | |
| startAny | `*_start_any` | Starts with any of | |
| startAll | `*_start_all` | Starts with all of | |
| notStartAny | `*_not_start_any` | Does not start with any of | |
| notStartAll | `*_not_start_all` | Does not start with all of | |
| end | `*_end` | Ends with | SQL: `col LIKE '%value'` |
| notEnd | `*_not_end` | Does not end with | |
| endAny | `*_end_any` | Ends with any of | |
| endAll | `*_end_all` | Ends with all of | |
| notEndAny | `*_not_end_any` | | |
| notEndAll | `*_not_end_all` | | |
| cont | `*_cont` | Contains value | uses `LIKE` |
| contAny | `*_cont_any` | Contains any of | |
| contAll | `*_cont_all` | Contains all of | |
| notCont | `*_not_cont` | Does not contain |
| notContAny | `*_not_cont_any` | Does not contain any of | |
| notContAll | `*_not_cont_all` | Does not contain all of | |
| true | `*_true` | is true | |
| false | `*_false` | is false | |


# Contributions

feel free to add issues and PRs