import { QueryBuilderOptions } from './../interfaces/queryBuilderOptions.interface';

export class QueryBuilder {

  _prop: string = "";
  _q: {[key: string]: string} = {};

  constructor(options: QueryBuilderOptions[]) {
    options.forEach((option: QueryBuilderOptions) => {
      this._prop = option.property;
      Object.keys(option.filters).forEach(filter=> {
        if (typeof this[filter] === 'function') {
          this[filter](option.filters[filter]);
        }
      });
    });
  }

  private eq(value: string) {
    this._q[this._prop+'_eq'] = value;
  }

  private notEq(value: string) {
    this._q[this._prop+'_not_eq'] = value;
  }

  private matches(value: string) {
    this._q[this._prop+'_matches'] = value;
  }

  private doesNotMatch(value: string) {
    this._q[this._prop+'_does_not_match'] = value;
  }

  private matchesAny(value: string) {
    this._q[this._prop+'_matches_any'] = value;
  }

  private matchesAll(value: string) {
    this._q[this._prop+'_matches_all'] = value;
  }

  private doesNotMatchAny(value: string) {
    this._q[this._prop+'_does_not_match_any'] = value;
  }

  private doesNotMatchAll(value: string) {
    this._q[this._prop+'_does_not_match_all'] = value;
  }

  private lt(value: string) {
    this._q[this._prop+'_lt'] = value;
  }

  private ltEq(value: string) {
    this._q[this._prop+'_lteq'] = value;
  }

  private gt(value: string) {
    this._q[this._prop+'_gt'] = value;
  }

  private gtEq(value: string) {
    this._q[this._prop+'_gteq'] = value;
  }

  private present(value: string) {
    this._q[this._prop+'_present'] = value;
  }

  private blank(value: string) {
    this._q[this._prop+'_blank'] = value;
  }

  private null(value: string) {
    this._q[this._prop+'_null'] = value;
  }

  private notNull(value: string) {
    this._q[this._prop+'_not_null'] = value;
  }

  private in(value: string) {
    this._q[this._prop+'_in'] = value;
  }

  private notIn(value: string) {
    this._q[this._prop+'_not_in'] = value;
  }

  private ltAny(value: string) {
    this._q[this._prop+'_lt_any'] = value;
  }

  private ltEqAny(value: string) {
    this._q[this._prop+'_lt_eq_any'] = value;
  }

  private gtAny(value: string) {
    this._q[this._prop+'_gt_any'] = value;
  }

  private gtEqAny(value: string) {
    this._q[this._prop+'_gt_eq_any'] = value;
  }

  private ltAll(value: string) {
    this._q[this._prop+'_lt_all'] = value;
  }

  private ltEqAll(value: string) {
    this._q[this._prop+'_lt_eq_all'] = value;
  }

  private gtAll(value: string) {
    this._q[this._prop+'_gt_all'] = value;
  }

  private gtEqAll(value: string) {
    this._q[this._prop+'_gt_eq_all'] = value;
  }

  private notEqAll(value: string) {
    this._q[this._prop+'_not_eq_all'] = value;
  }

  private start(value: string) {
    this._q[this._prop+'_start'] = value;
  }

  private notStart(value: string) {
    this._q[this._prop+'_not_start'] = value;
  }

  private startAny(value: string) {
    this._q[this._prop+'_start_any'] = value;
  }

  private startAll(value: string) {
    this._q[this._prop+'_start_all'] = value;
  }

  private notStartAny(value: string) {
    this._q[this._prop+'_not_start_any'] = value;
  }

  private notStartAll(value: string) {
    this._q[this._prop+'_not_start_all'] = value;
  }

  private end(value: string) {
    this._q[this._prop+'_end'] = value;
  }

  private notEnd(value: string) {
    this._q[this._prop+'_not_end'] = value;
  }

  private endAny(value: string) {
    this._q[this._prop+'_end_any'] = value;
  }

  private endAll(value: string) {
    this._q[this._prop+'_end_all'] = value;
  }

  private notEndAny(value: string) {
    this._q[this._prop+'_not_end_any'] = value;
  }

  private notEndAll(value: string) {
    this._q[this._prop+'_not_end_all'] = value;
  }

  private cont(value: string) {
    this._q[this._prop+'_cont'] = value;
  }

  private contAny(value: string) {
    this._q[this._prop+'_cont_any'] = value;
  }

  private contAll(value: string) {
    this._q[this._prop+'_cont_all'] = value;
  }

  private notCont(value: string) {
    this._q[this._prop+'_not_cont'] = value;
  }

  private notContAny(value: string) {
    this._q[this._prop+'_not_cont_any'] = value;
  }

  private notContAll(value: string) {
    this._q[this._prop+'_not_cont_all'] = value;
  }

  private true(value: string) {
    this._q[this._prop+'_true'] = value;
  }

  private false(value: string) {
    this._q[this._prop+'_false'] = value;
  }

  public toObject() {
    return this._q
  }

  public toUrlParams() {
    return Object.entries(this._q)
      .map(([key, val]) => `q[${key}]=${encodeURIComponent(val)}`)
      .join('&');
  }
}
