export function or(...args: string[]): string {
  let _or: string = '';

  args.forEach((arg: string, index: number) => {
    if (index < args.length - 1) {
      _or += `${arg}_or_`
    } else {
      _or += arg;
    }
  })
  return _or;
}