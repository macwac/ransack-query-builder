type RequireAtLeastOne<T, Keys extends keyof T = keyof T> =
  Pick<T, Exclude<keyof T, Keys>>
  & {
    [K in Keys]-?: Required<Pick<T, K>>
  }[Keys]

interface IQueryBuilder<T> {
  property: string;
  filters: T;
}

interface IQueryBuilderFilter {
  eq: string;
  notEq: string;
  matches: string;
  doesNotMatch: string;
  matchesAny: string;
  matchesAll: string;
  doesNotMatchAny: string;
  doesNotMatchAll: string;
  lt: string;
  ltEq: string;
  gt: string;
  gtEq: string;
  present: string;
  blank: string;
  null: string;
  notNull: string;
  in: string;
  notIn: string;
  ltAny: string;
  ltEqAny: string;
  gtAny: string;
  gtEqAny: string;
  ltAll: string;
  ltEqAll: string;
  gtAll: string;
  gtEqAll: string;
  notEqAll: string;
  start: string;
  notStart: string;
  startAny: string;
  startAll: string;
  notStartAny: string;
  notStartAll: string;
  end: string;
  notEnd: string;
  endAny: string;
  endAll: string;
  notEndAny: string;
  notEndAll: string;
  cont: string;
  contAny: string;
  contAll: string;
  notCont: string;
  notContAny: string;
  notContAll: string;
  true: string;
  false: string;
}

export type QueryBuilderOptions = IQueryBuilder<RequireAtLeastOne<IQueryBuilderFilter,
  'eq' |
  'notEq' |
  'matches' |
  'doesNotMatch' |
  'matchesAny' |
  'matchesAll' |
  'doesNotMatchAny' |
  'doesNotMatchAll' |
  'lt' |
  'ltEq' |
  'gt' |
  'gtEq' |
  'present' |
  'blank' |
  'null' |
  'notNull' |
  'in' |
  'notIn' |
  'ltAny' |
  'ltEqAny' |
  'gtAny' |
  'gtEqAny' |
  'ltAll' |
  'ltEqAll' |
  'gtAll' |
  'gtEqAll' |
  'notEqAll' |
  'start' |
  'notStart' |
  'startAny' |
  'startAll' |
  'notStartAny' |
  'notStartAll' |
  'end' |
  'notEnd' |
  'endAny' |
  'endAll' |
  'notEndAny' |
  'notEndAll' |
  'cont' |
  'contAny' |
  'contAll' |
  'notCont' |
  'notContAny' |
  'notContAll' |
  'true' |
  'false'
>>
