import { QueryBuilder, QueryBuilderOptions, or } from './../lib';

let options: QueryBuilderOptions[] = [
  {
    property: or('date', 'time'),
    filters: {
      eq: '12-12-2020',
    }
  },
  {
    property: 'time',
    filters: {
      notEq: '13-01-2019',
    }
  }
]

let q = new QueryBuilder(options)
console.log({ q: q.toObject() });
console.log( q.toUrlParams() );
