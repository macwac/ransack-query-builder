"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class QueryBuilder {
    constructor(options) {
        this._prop = "";
        this._q = {};
        options.forEach((option) => {
            this._prop = option.property;
            Object.keys(option.filters).forEach(filter => {
                if (typeof this[filter] === 'function') {
                    this[filter](option.filters[filter]);
                }
            });
        });
    }
    eq(value) {
        this._q[this._prop + '_eq'] = value;
    }
    notEq(value) {
        this._q[this._prop + '_not_eq'] = value;
    }
    matches(value) {
        this._q[this._prop + '_matches'] = value;
    }
    doesNotMatch(value) {
        this._q[this._prop + '_does_not_match'] = value;
    }
    matchesAny(value) {
        this._q[this._prop + '_matches_any'] = value;
    }
    matchesAll(value) {
        this._q[this._prop + '_matches_all'] = value;
    }
    doesNotMatchAny(value) {
        this._q[this._prop + '_does_not_match_any'] = value;
    }
    doesNotMatchAll(value) {
        this._q[this._prop + '_does_not_match_all'] = value;
    }
    lt(value) {
        this._q[this._prop + '_lt'] = value;
    }
    ltEq(value) {
        this._q[this._prop + '_lteq'] = value;
    }
    gt(value) {
        this._q[this._prop + '_gt'] = value;
    }
    gtEq(value) {
        this._q[this._prop + '_gteq'] = value;
    }
    present(value) {
        this._q[this._prop + '_present'] = value;
    }
    blank(value) {
        this._q[this._prop + '_blank'] = value;
    }
    null(value) {
        this._q[this._prop + '_null'] = value;
    }
    notNull(value) {
        this._q[this._prop + '_not_null'] = value;
    }
    in(value) {
        this._q[this._prop + '_in'] = value;
    }
    notIn(value) {
        this._q[this._prop + '_not_in'] = value;
    }
    ltAny(value) {
        this._q[this._prop + '_lt_any'] = value;
    }
    ltEqAny(value) {
        this._q[this._prop + '_lt_eq_any'] = value;
    }
    gtAny(value) {
        this._q[this._prop + '_gt_any'] = value;
    }
    gtEqAny(value) {
        this._q[this._prop + '_gt_eq_any'] = value;
    }
    ltAll(value) {
        this._q[this._prop + '_lt_all'] = value;
    }
    ltEqAll(value) {
        this._q[this._prop + '_lt_eq_all'] = value;
    }
    gtAll(value) {
        this._q[this._prop + '_gt_all'] = value;
    }
    gtEqAll(value) {
        this._q[this._prop + '_gt_eq_all'] = value;
    }
    notEqAll(value) {
        this._q[this._prop + '_not_eq_all'] = value;
    }
    start(value) {
        this._q[this._prop + '_start'] = value;
    }
    notStart(value) {
        this._q[this._prop + '_not_start'] = value;
    }
    startAny(value) {
        this._q[this._prop + '_start_any'] = value;
    }
    startAll(value) {
        this._q[this._prop + '_start_all'] = value;
    }
    notStartAny(value) {
        this._q[this._prop + '_not_start_any'] = value;
    }
    notStartAll(value) {
        this._q[this._prop + '_not_start_all'] = value;
    }
    end(value) {
        this._q[this._prop + '_end'] = value;
    }
    notEnd(value) {
        this._q[this._prop + '_not_end'] = value;
    }
    endAny(value) {
        this._q[this._prop + '_end_any'] = value;
    }
    endAll(value) {
        this._q[this._prop + '_end_all'] = value;
    }
    notEndAny(value) {
        this._q[this._prop + '_not_end_any'] = value;
    }
    notEndAll(value) {
        this._q[this._prop + '_not_end_all'] = value;
    }
    cont(value) {
        this._q[this._prop + '_cont'] = value;
    }
    contAny(value) {
        this._q[this._prop + '_cont_any'] = value;
    }
    contAll(value) {
        this._q[this._prop + '_cont_all'] = value;
    }
    notCont(value) {
        this._q[this._prop + '_not_cont'] = value;
    }
    notContAny(value) {
        this._q[this._prop + '_not_cont_any'] = value;
    }
    notContAll(value) {
        this._q[this._prop + '_not_cont_all'] = value;
    }
    true(value) {
        this._q[this._prop + '_true'] = value;
    }
    false(value) {
        this._q[this._prop + '_false'] = value;
    }
    toObject() {
        return this._q;
    }
    toUrlParams() {
        return Object.entries(this._q)
            .map(([key, val]) => `q[${key}]=${encodeURIComponent(val)}`)
            .join('&');
    }
}
exports.QueryBuilder = QueryBuilder;
