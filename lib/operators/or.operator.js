"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function or(...args) {
    let _or = '';
    args.forEach((arg, index) => {
        if (index < args.length - 1) {
            _or += `${arg}_or_`;
        }
        else {
            _or += arg;
        }
    });
    return _or;
}
exports.or = or;
