# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.0.1]
### Changed
- Initial Version

## [0.0.2]
### Fixed
- fix readme

## [0.0.3]
### Added
- add types to package.json

## [0.0.4]
### fixed
- fix npmignore

## [0.0.5]
### fixed
- Again fix npm ignore ;__;

# [0.0.6]
### added
- add `or` operator
